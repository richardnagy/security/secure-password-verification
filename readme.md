# Secure Password Verification

A secure way to log in using an identification and password without the password being sent on the network or known by the server.